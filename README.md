# Razvoj na strežniku z uporabo Node.js

3\. vaje pri predmetu [Osnove informacijskih sistemov](https://ucilnica1617.fri.uni-lj.si/course/view.php?id=54) (navodila)




## Spletna storitev FriBox

Repozitorij vsebuje nedelujočo spletno storitev FriBox. FriBox predstavlja napredno storitev za oblačno shranjevanje in deljenje datotek. Kot izvrstna spletna aplikacija posega na izjemno konkurenčno področje obstoječih tovrstnih rešitev. V okviru vaj ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na sliki spodaj. Med delom smiselno uveljavljajte spremembe v lokalnem in oddaljenem repozitoriju!

![FriBox](fribox.gif)